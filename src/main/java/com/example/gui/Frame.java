package com.example.gui;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.example.demo.AlbumImage;
import com.example.demo.ImageService;

import com.google.common.io.Files;

import javax.swing.JButton;


public class Frame extends JFrame{
	
	private JButton downloadButton;
	private JButton cleanButton;
	private JButton removeButton;
	private JLabel imageLabel;
	private JLabel statusLabel;
	
	private ImageService is;
	
	private String imageId;
	
	public void setImageService(ImageService ims) {
		is = ims;
	}
	public Frame() {
		initComponents();
	}
	
	
	public String getImageURL () {
		return "https://images.freeimages.com/images/large-previews/ed3/a-stormy-paradise-1-1563744.jpg";
	}
	public void initComponents() {	
		imageLabel = new JLabel();
		statusLabel = new JLabel();
		
		downloadButton = new JButton();
		downloadButton.setText("Download");
		
		downloadButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				statusLabel.setText("Descargando Imagen");
				String imageUrl = getImageURL();
				imageId = imageUrl;
				AlbumImage im = is.getAlbumImage(imageUrl);
				imageLabel.setIcon(im.getImage());
			}
		});
		
		cleanButton = new JButton();
		cleanButton.setText("Clean");
		cleanButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				statusLabel.setText("CleanClicked");
				imageLabel.setIcon(null);
			}
		});
		
		removeButton = new JButton();
		removeButton.setText("Remove");
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				statusLabel.setText("RemoveClicked");
				imageLabel.setIcon(null);
				is.removeAlbumImage(imageId);
			}
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(downloadButton)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(cleanButton)
                        	.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        	.addComponent(removeButton))
                        .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(151, Short.MAX_VALUE))
            );
            layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(downloadButton)
                        .addComponent(cleanButton)
                        .addComponent(removeButton))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 129, Short.MAX_VALUE)
                    .addComponent(statusLabel)
                    .addContainerGap())
            );
	    pack();
	}
	
	
	
	
	
}
